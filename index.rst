.. AMS209 Website documentation master file, created by
   sphinx-quickstart on Sun Oct 23 02:56:10 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

AMS209 - Foundations of Scientific Computing
============================================

Here I will post my progress throughout the course.

Contents:

.. toctree::
   :maxdepth: 2

   homeworks
   temp
   code

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

